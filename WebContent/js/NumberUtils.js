function isNumber(value)
{
	return /^[-+]?\d+(\.\d+)?$/.test(value);
}

function isPositiveNumber(value)
{
	return /^[+]?\d+(\.\d+)?$/.test(value);
}

function isNegativeNumber(value)
{
	return /^-\d+(\.\d+)?$/.test(value);
}

function isInteger(value)
{
	return /^[+-]?\d+$/.test(value);
}

function isPositiveInteger(value)
{
	return /^(\+)?\d+$/.test(value);
}

function isNegativeInteger(value)
{
	return /^\-\d+$/.test(value);
}

function isDigit(value)
{
	return /^\d$/.test(value);
}

function isUsCurrency(value)
{
	return /^\$?([1-9]{1}[0-9]{0,2}(\,[0-9]{3})*(\.[0-9]{0,2})?|[1-9]{1}[0-9]{0,}(\.[0-9]{0,2})?|0(\.[0-9]{0,2})?|(\.[0-9]{1,2})?)$/.test(value);
}

//保留两位小数    
//功能：将浮点数四舍五入，取小数点后2位   
function toDecimal(x,n) 
{   
	var temp = Math.pow(10,n);
    var f = parseFloat(x);   
    if (isNaN(f)) 
    {   
        return 0;   
    }   
    f = Math.round(x*temp)/temp;   
    return f;   
}   


//制保留2位小数，如：2，会在2后面补上00.即2.00   
function toDecimal2(x,n) 
{   
	var temp = Math.pow(10,n);
    var f = parseFloat(x);   
    if (isNaN(f)) 
    {   
        return 0;   
    }   
    var f = Math.round(x*temp)/temp;   
    var s = f.toString();   
    var rs = s.indexOf('.');   
    if (rs < 0) 
    {   
        rs = s.length;   
        s += '.';   
    }   
    while (s.length <= rs + n) 
    {   
        s += '0';   
    }   
    return s;   
}   







