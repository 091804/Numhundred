
//base
var _baseUrl = './';
var baseJSUrl = './themes/style/dk/js/';
var baseCSSUrl = './themes/style/dk/css/';

var account_url="./user.php";
var recentlyViewedUrl="./productExt/index/getViewedItemsJson/"; //最近浏览
var saveMessageUrl="./account/message/saveAjax/";//提交留言
var checkout_Url="./flow.php?step=checkout";//去购物车并结算
var go_buy="/";//购买
var place_url="themes/style/images/place.png";//loading图片
//Dialog icon base path
ds.Dialog.defaults.iconBasePath = 'themes/style/dk/images/';
function showInfoTip(content, type, onhide){
	var icons = {success:'success.png', info: 'info.png'};
	var icon = icons[type] || 'error.png';
	return ds.dialog({
		title: '消息提示',
		content: content,
		onhide: onhide,
		onyes: true,
		icon: icon,
		timeout: 15,
		width: 600
	});
}

function showInfoTip1(content, type, onhide){
	var icons = {success:'success.png', info: 'info.png'};
	var icon = icons[type] || 'error.png';
	return ds.dialog({
		title: '消息提示',
		content: content,
		onhide: onhide,
		onyes: true,
		icon: icon,
		width: 600
	});
}
function showInfoTip2(content, type, onhide){
	var icons = {success:'success.png', info: 'info.png', down: 'down.png'};
	var icon = icons[type] || 'error.png';
	return ds.dialog({
		title: '消息提示',
		content: content,
		onhide: onhide,
		onyes: true,
		icon: icon,
		width: 600
	});
}
function showInfoTip4(content, type, onhide){
	var icons = {success:'success.png', info: 'info.png'};
	var icon = icons[type] || 'down.png';
	return ds.dialog({
		title: '消息提示',
		content: content,
		onhide: onhide,
		onyes: true,
		icon: icon,
		timeout: 15,
		width: 600
	});
}
//购物车
jQuery(function($){
	var 
	shell = $('#header_cartlist'),
	noListTmpl = '<div class="no_product"><i class="big_cart"></i><span>您的购物车中没有商品，<a href="/catalog.php">马上购买</a></span></div>',
	listTmpl = '<ul><%for(var i=0,len=list.length; i<len; i++){%>';
	listTmpl += '<li><a href="<%=list[i].productUrl%>" target="_blank" class="pic"><img src="<%=list[i].productImage%>" height="60" width="60" alt="<%=list[i].productName%>" /></a><a href="<%=list[i].productUrl%>" target="_blank" class="tit"><%=list[i].productName%></a>';
	listTmpl += '<div class="prop"><em class="price" title="单价">&yen;<%=list[i].price%></em><span class="count">数量<em>×<%=list[i].qty%></em></span></div><a title="删除" class="del" data-id="<%=list[i].id%>" onclick="drop_goods_cart(<%=list[i].id%>)"  href="javascript:;" >×</a>';
	
	listTmpl += '<%}%></ul><div class="user_cart_funs">';
	listTmpl += '<div class="total">共有<em><%=cartQty%></em>件商品  购物车小计：<em class="price">&yen;<%=subtotal%></em></div>';
	listTmpl += '<div class="btns"><a href="/flow.php" class="btn"><span>去购物车并结算</span></a></div>';
	listTmpl += '</div>';

	function getCartListHTML(data){
		var html = noListTmpl;
		if(data && data.list && data.list.length > 0){
			html = ds.tmpl(listTmpl, data);
		}
		return html;
	}
	
	function setCartListHTML(data){
		if(data && data.list){
			shell.html(getCartListHTML(data));
			$('#cart_subtotal').html('&yen;' + data.subtotal);
			$('#cart_qty').html(data.cartQty);
			$('#cartNum').html(data.cartQty);
		}
	}

	function getCartList(callback){
		
		return $.ajax({
			url: '/flow.php',
			dataType: 'json',
			data: {step:"wd_get_cart"},
			cache: false,
			success: function(data){
				//console.log(callback);
				(callback || $.noop)(data);
			},
			error:function(data){
				//console.log(data);
			}
		});
	}
	
	function refreshDisplayCart(data){
		if(data && data.info){
			setCartListHTML(data.info);
			return;
		}
		return getCartList(function(data){
			if(data && data.info && data.success == '1'){
				setCartListHTML(data.info);
			}
		});
	}

	//删除购物车
	shell.delegate('a.del', 'click', function(e){
		e.preventDefault();

		$.ajax({
			url: '/flow.php?step=drop_goods_cart',
			data: {id: this.getAttribute('data-id')},
			dataType: 'json',
			cache: false,
			success: function(data){
				if(data && data.success != '1' && data.info){
					ds.dialog.tips(data.info, 3);
				}
			},
			complete: refreshDisplayCart
		});
		$(this.parentNode).remove();
	});

	//手动刷新
	var cartRefreshTimer;
	shell.parent().hover(function(){
		clearTimeout(cartRefreshTimer);
		cartRefreshTimer = setTimeout(refreshDisplayCart, 150);
	}, function(){
		clearTimeout(cartRefreshTimer);
	});
	refreshDisplayCart();

	//挂载 & 初始化
	ds.mix(window, {
		refreshDisplayCart: refreshDisplayCart,
		getCartListHTML: getCartListHTML,
		setCartListHTML: setCartListHTML,
		getCartList: getCartList
	});
});
//Quick Links, with screen.width>=1336
if(window.screen && screen.width >= 1336){
	ds.loadScript(baseJSUrl+'quick_links.js');
}
//Navigator autoFixed
ds.loadScript('./themes/style/dk/js/jquery.autoFixed.js', function(){
	$('#navigator_inner').autoFixed();
	var op = new Object();
	op.offset=40;
	$('#cart_txt').autoFixed(op);
});





$(function(){ 
    //if($(this).scrollTop() <= 0)
    //$("#toTop").hide();
  //回到顶部
  $(window).scroll(function(){
    if($(this).scrollTop() > 0){
      $("#toTop").fadeIn();
    }else{
      $("#toTop").fadeOut();
    }
  });
  $("#toTop").click(function(){
    $("html,body").animate({scrollTop:"0"},500);
    return false;
  });

  //客服
  $('.kfBut').hover(function(){
  	//$(this).find('.kfqq_box').fadeIn();	
  	$(this).find('.kfqq_box').animate({width:"213px",left:"-213px"}, 200 );	
  },function(){
  	//$(this).find('.kfqq_box').fadeOut();	
  	$(this).find('.kfqq_box').animate({width:"0px",left:"0px"}, 200 );	
  });

})

 function closeGuide()
        {
        document.getElementById('guideBar').style.display="none";
        }