/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function addCombineProductToCart(parentId)
{
    var c_id=0;
    var arr=new Array;
    $("#sale-combine-list input[type=checkbox]").each(function(k){
        if($(this).attr('checked'))
        {
            arr[c_id]=$(this).val();
            c_id++;
        }
    });
    var str=arr.join(",");
    
   parentId   = (typeof(parentId) == "undefined") ? 0 : parseInt(parentId);

  Ajax.call('flow.php?step=add_combine_to_cart', 'goodids=' +str+'&parentId='+parentId , addCombineProductToCartResponse, 'POST', 'JSON');
}

/* *
 * 处理添加商品到购物车的反馈信息
 */
function addCombineProductToCartResponse(result)
{
  if (result.error > 0)
  {
    // 如果需要缺货登记，跳转
    if (result.error == 2)
    {
      if (confirm(result.message))
      {
        location.href = 'user.php?act=add_booking&id=' + result.goods_id + '&spec=' + result.product_spec;
      }
    }
    // 没选规格，弹出属性选择框
    else if (result.error == 6)
    {
      openSpeDiv(result.message, result.goods_id, result.parent);
    }
    else
    {
      alert(result.message);
    }
  }
  else
  {
    var cartInfo = document.getElementById('ECS_CARTINFO');
    var cart_url = 'flow.php?step=checkout';
    if (cartInfo)
    {
      cartInfo.innerHTML = result.content;
    }

    if (result.one_step_buy == '1')
    {
      location.href = cart_url;
    }
    else
    {
      switch(result.confirm_type)
      {
        case '1' :
          if (confirm(result.message)) location.href = cart_url;
          break;
        case '2' :
          if (!confirm(result.message)) location.href = cart_url;
          break;
        case '3' :
          location.href = cart_url;
          break;
        default :
			 location.href = cart_url;
          break;
      }
    }
  }
}

function getCombinePrice(p_id)
{
    var c_id=0;
    var arr=new Array;
    $("#sale-combine-list input[type=checkbox]").each(function(k){
        if($(this).attr('checked'))
        {
            arr[c_id]=$(this).val();
            c_id++;
        }
    });
    var str=arr.join(",");
    Ajax.call('goods.php', 'act=pack_price&pid=' + p_id+'&cid='+str, CombinePriceResponse, 'GET', 'JSON',true,true);
}

function CombinePriceResponse(res)
{
  if (res.err_msg.length > 0)
  {
    alert(res.err_msg);
  }
  else
  {
    $("#yuan-price").html(res.yuan_price_format);  
    $("#new-price").html(res.shop_price_format);
    $("#save-price").html(res.save_price_format);
  }
}
