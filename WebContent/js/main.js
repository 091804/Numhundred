/// <reference path="../Scripts/jquery-2.0.3-vsdoc.js" />
//延时加载
!function (a, b, c, d) { var e = a(b); a.fn.lazyload = function (f) { function g() { var b = 0; i.each(function () { var c = a(this); if (!j.skip_invisible || c.is(":visible")) if (a.abovethetop(this, j) || a.leftofbegin(this, j)); else if (a.belowthefold(this, j) || a.rightoffold(this, j)) { if (++b > j.failure_limit) return !1 } else c.trigger("appear"), b = 0 }) } var h, i = this, j = { threshold: 0, failure_limit: 0, event: "scroll", effect: "show", container: b, data_attribute: "original", skip_invisible: !0, appear: null, load: null, placeholder: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" }; return f && (d !== f.failurelimit && (f.failure_limit = f.failurelimit, delete f.failurelimit), d !== f.effectspeed && (f.effect_speed = f.effectspeed, delete f.effectspeed), a.extend(j, f)), h = j.container === d || j.container === b ? e : a(j.container), 0 === j.event.indexOf("scroll") && h.bind(j.event, function () { return g() }), this.each(function () { var b = this, c = a(b); b.loaded = !1, (c.attr("src") === d || c.attr("src") === !1) && c.attr("src", j.placeholder), c.one("appear", function () { if (!this.loaded) { if (j.appear) { var d = i.length; j.appear.call(b, d, j) } a("<img />").bind("load", function () { var d = c.data(j.data_attribute); c.hide(), c.is("img") ? c.attr("src", d) : c.css("background-image", "url('" + d + "')"), c[j.effect](j.effect_speed), b.loaded = !0; var e = a.grep(i, function (a) { return !a.loaded }); if (i = a(e), j.load) { var f = i.length; j.load.call(b, f, j) } }).attr("src", c.data(j.data_attribute)) } }), 0 !== j.event.indexOf("scroll") && c.bind(j.event, function () { b.loaded || c.trigger("appear") }) }), e.bind("resize", function () { g() }), /iphone|ipod|ipad.*os 5/gi.test(navigator.appVersion) && e.bind("pageshow", function (b) { b.originalEvent && b.originalEvent.persisted && i.each(function () { a(this).trigger("appear") }) }), a(c).ready(function () { g() }), this }, a.belowthefold = function (c, f) { var g; return g = f.container === d || f.container === b ? (b.innerHeight ? b.innerHeight : e.height()) + e.scrollTop() : a(f.container).offset().top + a(f.container).height(), g <= a(c).offset().top - f.threshold }, a.rightoffold = function (c, f) { var g; return g = f.container === d || f.container === b ? e.width() + e.scrollLeft() : a(f.container).offset().left + a(f.container).width(), g <= a(c).offset().left - f.threshold }, a.abovethetop = function (c, f) { var g; return g = f.container === d || f.container === b ? e.scrollTop() : a(f.container).offset().top, g >= a(c).offset().top + f.threshold + a(c).height() }, a.leftofbegin = function (c, f) { var g; return g = f.container === d || f.container === b ? e.scrollLeft() : a(f.container).offset().left, g >= a(c).offset().left + f.threshold + a(c).width() }, a.inviewport = function (b, c) { return !(a.rightoffold(b, c) || a.leftofbegin(b, c) || a.belowthefold(b, c) || a.abovethetop(b, c)) }, a.extend(a.expr[":"], { "below-the-fold": function (b) { return a.belowthefold(b, { threshold: 0 }) }, "above-the-top": function (b) { return !a.belowthefold(b, { threshold: 0 }) }, "right-of-screen": function (b) { return a.rightoffold(b, { threshold: 0 }) }, "left-of-screen": function (b) { return !a.rightoffold(b, { threshold: 0 }) }, "in-viewport": function (b) { return a.inviewport(b, { threshold: 0 }) }, "above-the-fold": function (b) { return !a.belowthefold(b, { threshold: 0 }) }, "right-of-fold": function (b) { return a.rightoffold(b, { threshold: 0 }) }, "left-of-fold": function (b) { return !a.rightoffold(b, { threshold: 0 }) } }) } (jQuery, window, document);

/*首页焦点图下面滚动*/
jQuery.fn.imageScroller = function (params) {
    var p = params || {
        next: "buttonNext",
        prev: "buttonPrev",
        frame: "viewerFrame",
        width: 100,
        child: "a",
        auto: true,
        smpic: false
    };
    var _btnNext = $("#" + p.next);
    var _btnPrev = $("#" + p.prev);
    var _imgFrame = $("#" + p.frame);
    var _width = p.width;
    var _child = p.child;
    var _auto = p.auto;
    var _itv;

    var turnLeft = function(){
        _btnPrev.unbind("click", turnLeft);
        if (_auto) autoStop();
        _imgFrame.animate({ marginLeft: -_width }, 'fast', function () {
            _imgFrame.find(_child + ":first").appendTo(_imgFrame);
            _imgFrame.css("marginLeft", 0);
            _btnPrev.bind("click", turnLeft);
            if (_auto) autoPlay();
        });
    };

    var turnRight = function () {
        _btnNext.unbind("click", turnRight);
        if (_auto) autoStop();
        _imgFrame.find(_child + ":last").clone().fadeIn().prependTo(_imgFrame);
        if (p.smpic) $("#smallpic li a").unbind('mouseover').mouseover(function () { smpicMouse(this) });
        _imgFrame.css("marginLeft", -_width);
        _imgFrame.animate({ marginLeft: 0 }, 'fast', function () {
            _imgFrame.find(_child + ":last").remove();
            _btnNext.bind("click", turnRight);
            if (_auto) autoPlay();
        });
    };

    _btnNext.css("cursor", "hand").click(turnRight);
    _btnPrev.css("cursor", "hand").click(turnLeft);

    var autoPlay = function () {
        _itv = window.setInterval(turnLeft, 4000);
    };
    var autoStop = function () {
        _imgFrame.stop();
        window.clearInterval(_itv);
    };
    if (_auto) {
        autoPlay();
        _imgFrame.hover(function () { autoStop();}, function () { autoPlay(); });
    }

};

/*详情页显示大图事件*/
function smpicMouse(smpic){
    
    $(smpic).parents("li").addClass("smpichover").siblings().removeClass("smpichover");
    
    var cls = $(smpic).attr('class');
    
    $('.bigpic a').hide();
    $('.bigpic .'+cls).show();
    
    
    
    $('.bigpic .'+cls).find('img').attr('href', $(smpic).find("img").attr("big"));
    $('.bigpic .'+cls).find('img').attr('src', $(smpic).find("img").attr("mid"));
    $('.bigpic .'+cls).find('img').attr('rel', $(smpic).find("img").attr("ig"));
    //$('.bigpic .'+cls).find('img').attr('data', $(smpic).find("img").attr("big"));
    $('.bigpic .'+cls).addClass('item'); 
    
    /*
	$("#zoom1").attr('href', $(smpic).find("img").attr("big"));
    $(".jqzoom").attr('src', $(smpic).find("img").attr("mid"));
    $(".jqzoom").attr('rel', $(smpic).find("img").attr("big"));
    $(".jqzon").attr('data', $(smpic).find("img").attr("big"));
    */
}

/*详情页图片放大镜*/
(function ($) {
    $.fn.imagezoom = function (options) {
        var settings = {
            xzoom: 310,
            yzoom: 310,
            offset: 10,
            position: "BTR",
            preload: 1
        };
        if (options) {
            $.extend(settings, options);
        }

        var noalt = '';
        var self = this;

        $(this).bind("mouseenter", function (ev) {
            return false;
            var imageLeft = $(this).offset().left; //元素左边距
            var imageTop = $(this).offset().top; //元素顶边距
            var imageWidth = $(this).get(0).offsetWidth; //图片宽度
            var imageHeight = $(this).get(0).offsetHeight; //图片高度
            var boxLeft = $(this).parent().offset().left; //父框左边距
            var boxTop = $(this).parent().offset().top; //父框顶边距
            var boxWidth = $(this).parent().width(); //父框宽度
            var boxHeight = $(this).parent().height(); //父框高度
            noalt = $(this).attr("alt"); //图片标题
            var bigimage = $(this).attr("rel"); //大图地址
            $(this).attr("alt", ''); //清空图片alt
            if ($("div.zoomDiv").get().length == 0) {
                $(document.body).append("<div class='zoomDiv'><img class='bigimg' src='" + bigimage + "'/></div><div class='zoomMask'></div>"); //放大镜框及遮罩
            }
            if (settings.position == "BTR") {
                //如果超过了屏幕宽度 将放大镜放在右边
                if (boxLeft + boxWidth + settings.offset + settings.xzoom > screen.width) {
                    leftpos = boxLeft - settings.offset - settings.xzoom;
                } else {
                    leftpos = boxLeft + boxWidth + settings.offset;
                }
            } else {
                leftpos = imageLeft - settings.xzoom - settings.offset;
                if (leftpos < 0) {
                    leftpos = imageLeft + imageWidth + settings.offset;
                }
            }
            $("div.zoomDiv").css({ top: boxTop, left: leftpos });
            $("div.zoomDiv").width(settings.xzoom);
            $("div.zoomDiv").height(settings.yzoom);
            $("div.zoomDiv").show();

            $(this).css('cursor', 'crosshair');

            $(document.body).mousemove(function (e) {
                mouse = new MouseEvent(e);
                if (mouse.x < imageLeft || mouse.x > imageLeft + imageWidth || mouse.y < imageTop || mouse.y > imageTop + imageHeight) {
                    mouseOutImage();
                    return;
                }

                var bigwidth = $(".bigimg").get(0).offsetWidth;
                var bigheight = $(".bigimg").get(0).offsetHeight;

                var scaley = 'x';
                var scalex = 'y';

                //设置遮罩层尺寸
                if (isNaN(scalex) | isNaN(scaley)) {
                    var scalex = (bigwidth / imageWidth);
                    var scaley = (bigheight / imageHeight);
                    $("div.zoomMask").width((settings.xzoom) / scalex);
                    $("div.zoomMask").height((settings.yzoom) / scaley);
                    $("div.zoomMask").css('visibility', 'visible');
                }

                xpos = mouse.x - $("div.zoomMask").width() / 2;
                ypos = mouse.y - $("div.zoomMask").height() / 2;

                xposs = mouse.x - $("div.zoomMask").width() / 2 - imageLeft;
                yposs = mouse.y - $("div.zoomMask").height() / 2 - imageTop;

                xpos = (mouse.x - $("div.zoomMask").width() / 2 < imageLeft) ? imageLeft : (mouse.x + $("div.zoomMask").width() / 2 > imageWidth + imageLeft) ? (imageWidth + imageLeft - $("div.zoomMask").width()) : xpos;
                ypos = (mouse.y - $("div.zoomMask").height() / 2 < imageTop) ? imageTop : (mouse.y + $("div.zoomMask").height() / 2 > imageHeight + imageTop) ? (imageHeight + imageTop - $("div.zoomMask").height()) : ypos;


                $("div.zoomMask").css({ top: ypos, left: xpos });
                $("div.zoomDiv").get(0).scrollLeft = xposs * scalex;
                $("div.zoomDiv").get(0).scrollTop = yposs * scaley;
            });
        });
        function mouseOutImage() {
            $(self).attr("alt", noalt);
            $(document.body).unbind("mousemove");
            $("div.zoomMask").remove();
            $("div.zoomDiv").remove();
        }

        //预加载
        count = 0;
        if (settings.preload) {
            $('body').append("<div style='display:none;' class='jqPreload" + count + "'></div>");

            $(this).each(function () {

                var imagetopreload = $(this).attr("rel");

                var content = jQuery('div.jqPreload' + count + '').html();

                jQuery('div.jqPreload' + count + '').html(content + '<img src=\"' + imagetopreload + '\">');

            });
        }
    }

})(jQuery);
function MouseEvent(e) {
    this.x = e.pageX;
    this.y = e.pageY;
}
/* $(function () {
     $("img.lazy").lazyload({
         effect: "fadeIn",
         skip_invisible: false,
         threshold: 200
     });
 });*/

