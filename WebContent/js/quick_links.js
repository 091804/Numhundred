/**
 * 右侧快速操作
 * kongge@office.weiphone.com
 * 2012.06.07
*/
jQuery(function($){
	//创建DOM
	var 
	quickHTML = '<div id="quick_links" class="quick_links"><a href="#top" class="return_top"><i class="top"></i><span>返回顶部</span></a><a href="'+account_url+'"><i class="setting"></i><span>个人中心</span></a><a href="#" class="cart_list"><i class="cart"></i><span>购物车</span></a><a href="#" class="history_list"><i class="view"></i><span>最近浏览</span></a><a href="#" class="leave_message"><i class="qa"></i><span>联系客服</span></a></div><div id="quick_links_pop" class="quick_links_pop hide"></div><div class="quick_toggle"><a href="javascript:;" class="toggle" title="展开/收起">×</a></div>',
	quickPanel = $(document.createElement('div'));
	quickPanel.addClass('quick_links_wrap').html(quickHTML).appendTo('body');
	
	//具体数据操作 
	var 
	quickPopXHR,
	loadingTmpl = '<div class="loading" style="padding:30px 80px"><i></i><span>Loading...</span></div>',
	popTmpl = '<div class="title"><h3><i></i><%=title%></h3></div><div class="pop_panel"><%=content%></div><div class="arrow"><i></i></div><div class="fix_bg"></div>',
	historyListTmpl = '<ul><%for(var i=0,len=items.length; i<5&&i<len; i++){%><li><a href="<%=items[i].productUrl%>" target="_blank" class="pic"><img alt="<%=items[i].productName%>" src="<%=items[i].productImage%>" width="60" height="60"/></a><a href="<%=items[i].productUrl%>" title="<%=items[i].productName%>" target="_blank" class="tit"><%=items[i].productName%></a><div class="price" title="单价"><em>&yen;<%=items[i].productPrice%></em></div></li><%}%></ul>',
	quickPop = quickPanel.find('#quick_links_pop'),
	quickDataFns = {
		//购物车
		cart_list: {
			title: '购物车',
			content: loadingTmpl,
			init: function(ops){
				//获取实时购物车
				
				
			
				var xx= function(data){	 
				 	
					if( data && data.info && data.success == '1'){
						var self = this;
						quickPop.html(ds.tmpl(popTmpl, {
						title: ops.title,
						content: '<div class="user_cart_inner">'+ getCartListHTML(data.info) +'</div>'
						}));
							quickPop.find('a.del').bind('click', function(e){
									e.preventDefault();
									$.ajax({
									url: "/flow.php",
									data: {id: this.getAttribute('data-id'),step:"drop_goods"},
									dataType: 'json',
									cache: false,
									success: function(data){
									if(data && data.success != '1' && data.info){
									ds.dialog.tips(data.info, 3);
									}
									},
									complete: function(){
									ops.init.call(self, ops);
									}
									});
								$(this.parentNode).remove();
							});
						setCartListHTML(data.info);
					}
				}
				 quickPopXHR = getCartList(xx); 
				 
				  
			}
		},
		//最近浏览
		history_list: {
			title: '最近浏览',
			content: loadingTmpl,
			init: function(ops){
				//获取实时最近浏览
				quickPopXHR = $.ajax({
					url: "history.php",
					dataType: 'json',
					cache: false,
					success: function(data){
						
						var html = '<div class="no_data"><i></i><span>没有浏览记录</span></div>';
						html="<ul>";
						for( var i=0; i< data.length; i++){
							var tt=eval("("+data[i]+")")
							html+='<li><a class="pic" target="_blank" href=""><img width="60" height="60" src="'+tt['productImage']+ '" alt=""></a><a class="tit" target="_blank" title="" href="'+tt['productUrl']+'">'+tt['productName']+'</a><div title="单价" class="price"><em>¥'+tt['productPrice']+'</em></div></li>';
						}
						html+="</ul>";
						
						
						quickPop.html(ds.tmpl(popTmpl, {
							title: ops.title,
							content: '<div class="slider related_slider history_slider"><div class="inner">'+ html +'</div></div>'
						}));
					}
				});
			}
		},
	 
		//给客服留言
		leave_message: {
			title: '给客服留言',
			content: '<form  name="formMsg" method="post" action="message.php"> <input type="hidden" value="" size="20" class="inputBg" name="user_email"><input type="hidden"  value="给客服留言" size="30" class="inputBg" name="msg_title"><table width="100%" cellpadding="3" border="0"><tbody><tr><td align="right">留言类型</td><td><input type="radio" checked="checked" value="0" name="msg_type">留言                  <input type="radio" value="1" name="msg_type"> 投诉                  <input type="radio" value="2" name="msg_type">询问                  <input type="radio" value="3" name="msg_type"> 售后                  <input type="radio" value="4" name="msg_type"> 求购 </td></tr>       <tr> <td valign="top" align="right">留言内容</td>    <td><textarea style="border:1px solid #ccc;" wrap="virtual" rows="4" cols="50" name="msg_content"></textarea></td>   </tr>  <tr> <td>&nbsp;</td>    <td><input type="hidden" value="ajax_act_add_message" name="act">    <div class="btns"><button type="submit" class="btn"><span>提交</span></button></div>  </td>    </tr>  </tbody></table> </form>',
			init: function(ops){
				setTimeout(function(){
					quickPop.find('textarea').focus();
				}, 100);
				//验证码
				//quickPop.find('#token_txt').bind('focus', getValidateCode);
				
				//效验 & 提交数据
				var form = quickPop.find('form');
				//form.attr("action",saveMessageUrl);
				form.bind('submit', function(e){
					
					
				var wd_data=form.formSerialize();
					
					e.preventDefault();
					var data = form.serialize();
					if(!checkMessageForm()){
						return false;
					}
					var type=quickPop.find(':radio:checked').val();
					jQuery.ajax({
						type:'post',
						url: 'message.php', 
						data:wd_data,
						dataType:"json",
						error:function(value){
							ds.dialog.alert('留言失败');
						},
						success: function(value){
							var success = value.success;
							var info = value.info;
							if(success==1){
								hideQuickPop();
								showInfoTip(info, 'success');
							}else{
								ds.dialog.alert(info);
							}
						}
					});
				});
			}
		}
	};
	
	//showQuickPop
	var 
	prevPopType,
	prevTrigger,
	doc = $(document),
	popDisplayed = false,
	hideQuickPop = function(){
		if(prevTrigger){
			prevTrigger.removeClass('current');
		}
		popDisplayed = false;
		prevPopType = '';
		quickPop.hide();
	},
	showQuickPop = function(type){
		if(quickPopXHR && quickPopXHR.abort){
			quickPopXHR.abort();
		}
		if(type !== prevPopType){
			var fn = quickDataFns[type];
			quickPop.html(ds.tmpl(popTmpl, fn));
			fn.init.call(this, fn);
		}
		doc.unbind('click.quick_links').one('click.quick_links', hideQuickPop);

		quickPop[0].className = 'quick_links_pop quick_' + type;
		popDisplayed = true;
		prevPopType = type;
		quickPop.show();
	};
	quickPanel.bind('click.quick_links', function(e){
		e.stopPropagation();
	});

	//通用事件处理
	var 
	view = $(window),
	getHandlerType = function(className){
		return className.replace(/current/g, '').replace(/\s+/, '');
	},
	showPopFn = function(){
		var type = getHandlerType(this.className);
		if(popDisplayed && type === prevPopType){
			return hideQuickPop();
		}
		showQuickPop(this.className);
		if(prevTrigger){
			prevTrigger.removeClass('current');
		}
		prevTrigger = $(this).addClass('current');
	},
	quickHandlers = {
		//购物车，最近浏览，商品咨询
		cart_list: showPopFn,
		history_list: showPopFn,
		leave_message: showPopFn,
		//返回顶部
		return_top: function(){
			this.style.display = 'none';
			ds.scrollTo(0, 0);
		}
	};
	quickPanel.delegate('a', 'click', function(e){
		var type = getHandlerType(this.className);
		if(type && quickHandlers[type]){
			quickHandlers[type].call(this);
			e.preventDefault();
		}
	});
	
	//Scroll Check
	var 
	scrollTimer,
	goTopBtn = quickPanel.find('a.return_top').hide(),
	checkScroll = function(){
		goTopBtn[view.scrollTop()>100 ? 'show' : 'hide']();
	};
	view.bind('scroll.go_top', function(){
		clearTimeout(scrollTimer);
		scrollTimer = setTimeout(checkScroll, 160);
	});
	

	//校验商品咨询表单
	function  checkMessageForm(){
		var content = $("#msg");
		if(content.val()==""){
			ds.dialog({
				   title : '消息',
				   content : "请填写咨询内容！",
				   onyes : function(){
						this.close();
				   },
				   width : 200,
				   lock : true
			});
			return false;
		}

		var checkcode = $("#token_txt").val();
		if(checkcode=="" || checkcode=="点击获取"){
			ds.dialog({
				   title : '消息',
				   content : "验证码不能为空，请输入验证码！",
				   onyes : function(){
						this.close();
				   },
				   width : 200,
				   lock : true
			});
			return false;
		}
		return true;
	}

	//获取验证码
	function getValidateCode(){
		this.value="";
		var validateCodeUrl = validateCode_url+'?t='+Math.random();
		$("#code_img").html('<img id="validate_code_img_id_1" src="' + validateCodeUrl + '" height="20" width="80" alt="验证码" />');
		return;
	}
});
