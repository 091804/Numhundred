function bindReply(id)
{
	$("#replyCustomerComment_" + id).toggle(),$("#reply_score_content_" + id).focus();
}
function customer_reply(id,pid,name)
{
	$("#replyCustomerComment_"+pid).removeClass('cmt_inputs_min');
	$("#reply_hidden_"+pid).val(id);
	$("#reply_score_content_"+pid).focus();
}
function comment_reply()
{
	$("#comment_div").removeClass('cmt_inputs_min');
}
function cancelReply(id)
{
	$("#reply_score_content_"+id).val('');
	$("#replyCustomerComment_"+id).addClass('cmt_inputs_min');
}

function cancelComment()
{   
	$("#comment_content").val('');
	$("#comment_div").addClass('cmt_inputs_min');
}
function getValidateCode(obj)
{
	var validateCodeUrl ='captcha.php?'+Math.random();
	obj.value="";
	var oo="this.src='captcha.php?'+Math.random()";
	$(obj).next().html('<img id="validate_code_img_id"  src="' + validateCodeUrl + '" height="30" width="100" alt="验证码" />');
	return;
}


//回复客户评论
function replyCustomerComment(obj,id)
{
	var content = $("#reply_score_content_"+id).val();
	var replyCode = $("#reply_score_code_"+id).val();
	
	if(content)
	{
		content = content.replace(/^\s*/, '')
	}
	if(content=="")
	{
		showInfoTip('回复内容不能为空！','info');
		return;
	}
	if(replyCode=="" || replyCode=="点击获取")
	{
		showInfoTip('请输入验证码！','info');
		return;
	}
	jQuery.ajax({
		type:'post',
		url: 'wd_ajax_goodbad.php?act=reply',
		data:{"code":replyCode,"content":content,"parentId":id},
		dataType:"json",
		cache: false,
		async: true,
		success: function(value)
		{
			if(value.error==0)
			{
				showInfoTip(value.message,'info');
				cancelReply(id);
			}
			else
			{
				showInfoTip(value.message,'info');
			}
		}
	});
}






function checkQty(obj)
{
	var qty = $(obj).val();
	if(!/[^0-9-]+/.test(qty))
	{
		qty = parseInt(qty);
		if(qty < 1)
		{
			$(obj).val(lastNum);
		}else
		{
			lastNum = qty;
		}
	}
	else
	{
		$(obj).val(lastNum);
	}
}

//有用没用
function commentUseful(id,category)
{
	$("#useful_"+id).replaceWith('<a id="useful_'+id+'" href="javascript:;" class="agree disabled"><span>支持(<em>'+$("#useful_"+id).children().children().html()+'</em>)</span></a>');
	$("#nouse_"+id).replaceWith('<a id="nouse_'+id+'" href="javascript:;" class="oppose disabled"><span>反对(<em>'+$("#nouse_"+id).children().children().html()+'</em>)</span></a>');
	if(category==1){
		type='good';
	}else{
		type='bad';
	}

	
	$.ajax({
		type:'post',
		url: "wd_ajax_goodbad.php",
		data:{"id":id,"category":category,"type":type},
		dataType:"json",
		cache: false,
		async: true,
		success: function(value)
		{
			
			if(value.success == 1)
			{
				if(category == '1')
				{
					
					$("#useful_"+id).replaceWith('<a id="useful_'+id+'" href="javascript:;" class="agree disabled"><span>支持(<em>'+value.info+'</em>)</span></a>');
					$("#nouse_"+id).replaceWith('<a id="nouse_'+id+'" href="javascript:;" class="oppose disabled"><span>反对(<em>'+$("#nouse_"+id).children().children().html()+'</em>)</span></a>');
				}
				if(category == '0')
				{
					$("#useful_"+id).replaceWith('<a id="useful_'+id+'" href="javascript:;" class="agree disabled"><span>支持(<em>'+$("#useful_"+id).children().children().html()+'</em>)</span></a>');
					$("#nouse_"+id).replaceWith('<a id="nouse_'+id+'" href="javascript:;" class="oppose disabled"><span>反对(<em>'+value.info+'</em>)</span></a>');
				}
			}
		
		}
	});	
	 
	
}



